@extends('admin.layouts.master')

@section('content')
    @push('css')
        <style>
            .mt-6{
                margin-top: 53px;
            }
        </style>
    @endpush
    <div class="row">
        <div class="col-md-6">
            <div class="min-height-200px">
                <div class="page-header">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="title">
                                <h4>Subject</h4>
                            </div>
                            <nav aria-label="breadcrumb" role="navigation">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Subject</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!-- Simple Datatable start -->
                <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
                    <div class="clearfix mb-20">
                        <div class="pull-left">
                            <h5 class="text-blue">Subject</h5>
                            <p class="font-14">SM Admin Panel</p>
                        </div>
                        <div class="pull-right">
                            <a href="{{ url('admin/subjects/create') }}" class="btn btn-sm btn-primary" role="button" title="Create">Add New</a>
                            <a href="{{ url('admin/subjects') }}" class="btn btn-sm btn-primary" role="button" title="Back">Back</a>
                        </div>
                    </div>
                    <div class="row">
                        <table class="data-table stripe hover nowrap">
                            <thead>
                            <tr>
                                <th class="table-plus datatable-nosort">Name</th>
                                <th>Age</th>
                                <th>Office</th>
                                <th>Address</th>
                                <th>Start Date</th>
                                <th class="datatable-nosort">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="table-plus">Andrea J. Cagle</td>
                                <td>30</td>
                                <td>Gemini</td>
                                <td>1280 Prospect Valley Road Long Beach, CA 90802 </td>
                                <td>29-03-2018</td>
                                <td>
                                    {{--                            <div class="dropdown">--}}
                                    {{--                                <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">--}}
                                    {{--                                    <i class="fa fa-ellipsis-h"></i>--}}
                                    {{--                                </a>--}}
                                    {{--                                <div class="dropdown-menu dropdown-menu-right">--}}
                                    {{--                                    <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View</a>--}}
                                    {{--                                    <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit</a>--}}
                                    {{--                                    <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete</a>--}}
                                    {{--                                </div>--}}
                                    {{--                            </div>--}}
                                    <ul class="list-inline">
                                        <li class="list-inline-item"><a href="#" class="btn btn-sm btn-primary" title="show"><i class="fa fa-eye"></i> </a> </li>
                                        <li class="list-inline-item"><a href="#" class="btn btn-sm btn-warning" title="edit"><i class="fa fa-pencil"></i> </a> </li>
                                        <li class="list-inline-item">
                                            {!! Form::open([ 'url' => [ 'admin/subjects', ], 'method' => 'delete' ]) !!}
                                            {!! Form::button("<i class='fa fa-trash'></i>",['type' => 'submit', 'onClick' => "return confirm('Are You Want to delete  ?')", 'class' => 'btn btn-sm btn-danger']) !!}
                                            {!! Form::close() !!}
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- Simple Datatable End -->
            </div>
        </div>
        <div class="col-md-6 mt-6">
            <div class="min-height-200px">
            <div class="page-header">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="title">
{{--                            <h4>Teacher Form</h4>--}}
                        </div>
                        <nav aria-label="breadcrumb" role="navigation">
                            <ol class="breadcrumb">
{{--                                <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>--}}
{{--                                <li class="breadcrumb-item active" aria-current="page">Create Subject</li>--}}
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            {!! Form::open([ 'url' => 'admin/subjects' ]) !!}
            @include('admin.subjects.form')
            {!! Form::close() !!}
        </div>
        </div>
    </div>

@endsection
