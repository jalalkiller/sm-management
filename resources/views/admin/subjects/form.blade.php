<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix">
        <div class="pull-left">
            <h4 class="text-blue">Subject Form</h4>
            <p class="mb-30 font-14">Subject</p>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="form-group col-md-12">
                    {!! Form::label('subject_name', 'Name <span class="text-danger">*</span>', [ "class" => "control-label {{ $errors->has('subject_name') ? 'has-error' : '' }}" ], false) !!}
                    {!! Form::text('subject_name', null, [ "class" => "form-control", "placeholder" => "Subject Name", 'required', 'autofocus' ]) !!}

                    @if($errors->has('subject_name'))
                        <span class="form-text">
                            <strong>{{ $errors->first('subject_name') }}</strong>
                        </span>
                    @endif

                    {!! Form::submit( 'Submit', [ "type" => "submit", "class" => "btn btn-primary mt-4" ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
