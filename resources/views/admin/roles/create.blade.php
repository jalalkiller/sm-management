@extends('admin.layouts.master')


@section('content')
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="title">
                        <h4>Role create</h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Form Basic</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <!-- horizontal Basic Forms Start -->
        <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
            <div class="clearfix">
                <div class="pull-left">
                    <h4 class="text-blue">Create Roles</h4>
                    <p class="mb-30 font-14">Admin Pnale for sm</p>
                </div>
                <div class="pull-right">
                    <a href="{{ url('admin/roles') }}" class="btn btn-primary"    role="button"><i class="fa fa-arrow-left"></i> Back</a>
                </div>
            </div>
            {!! Form::open(['url' => ['admin/roles'], 'method' => 'post']) !!}
                <div class="form-group">
                    {!! Form::label('name','Name',["class" => "font-weight-bold"]) !!}
                    {!! Form::text('name',null,["class" => "form-control",'required','autofocus',"placeholder" => "Role Name"]) !!}
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            {!! Form::label('permission','Permission',["class" => "weight-600 btn btn-info", "data-toggle"=>"collapse","data-target"=>"#demo"]) !!}
                            <div class="mb-5 collapse" id="demo">
                                    @foreach($permission as $row)
                                        <label>{{ Form::checkbox('permission[]', $row->id, false, ['class' => 'name']) }}
                                            {{ $row->name }}</label>
                                        <br/>
                                    @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group pull-right">
                        <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            {!! Form::close() !!}
        </div>
        <!-- horizontal Basic Forms End -->
    </div>

@endsection
