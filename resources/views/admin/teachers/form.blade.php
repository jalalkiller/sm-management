@push('css')
    <style>
        .mt-lg-10{
            margin-top: 100px;
        }
    </style>
@endpush
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix">
        <div class="pull-left">
            <h4 class="text-blue">Teacher Profile</h4>
            <p class="mb-30 font-14">Teacher</p>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="form-group col-md-6">
                    {!! Form::label('name', 'Name <span class="text-danger">*</span>', [ "class" => "control-label {{ $errors->has('name') ? 'has-error' : '' }}" ],false) !!}<!-- add tag inside lara collective form -->
                    {!! Form::text('name', null,[ "class" => "form-control", "placeholder" => "Name", 'required', 'autofocus' ] ) !!}

                    @if($errors->has('name'))
                        <span class="form-text">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif

                    {!! Form::label('dob', 'Date of birth <span class="text-danger">*</span>', [ "class" => "control-label {{ $errors->has('dob') ? 'has-error' : '' }} mt-2" ], false) !!}
                    {!! Form::date('dob', \Carbon\Carbon::now()->format('Y-m-d'), [ "class" => "form-control", "placeholder" => "Date of birth", 'required', 'autofocus']) !!}

                    @if($errors->has('dob'))
                        <span class="form-text">
                            <strong>{{ $errors->first('dob') }}</strong>
                        </span>
                    @endif

                    {!! Form::label('mobile', 'Mobile <span class="text-danger">*</span>', [ "class" => "control-label {{ $errors->has('mobile') ? 'has-error' : '' }} mt-2"], false) !!}
                    {!! Form::number('mobile', null, [ "class" => "form-control", "placeholder" => "Phone number", 'required', 'autofocus' ]) !!}

                    @if($errors->has('mobile'))
                        <span class="form-text">
                            <strong>{{ $errors->first('mobile') }}</strong>
                        </span>
                    @endif

                    {!! Form::label('email', 'E-mail <span class="text-danger">*</span>', [ "class" => "control-label {{ $errors->has('email) ? 'has-error' : '' }} mt-2" ], false) !!}
                    {!! Form::text('email', null, [ "class" => "form-control", "placeholder" => "E-mail", 'required', 'autofocus' ]) !!}

                    @if($errors->has('email'))
                        <span class="form-text">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                    {!! Form::label('address', 'Address', [ "class" => "control-label {{ $errors->has('address') ? 'has-error' : '' }} mt-2" ], false) !!}
                    {!! Form::text('address', null, [ "class" => "form-control", "placeholder" => "Address", 'autofocus' ]) !!}

                    @if($errors->has('address'))
                        <span class="form-text">
                            <strong>{{ $errors->first('address') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group col-md-6">
                    {!! Form::label('image', 'Image <span class="text-danger">*</span>', [ "class" => "control-label {{ $errors->has('image') ? 'has-error' : '' }}" ], false) !!}
                    <br>
                    {!! Form::file('image', null, [ "class" => "form-control",'id' => 'image', 'required','onchange'=>"loadFile(event)"]) !!}
{{--                    <div style="margin-top: 20px;">--}}
{{--                        <img id="output"--}}
{{--                             style="width: 100px;height: 100px;border-width: 4px 4px 4px 4px;border-style: solid;border-color: #ccc;"--}}
{{--                             src="{{ asset('/public/uploads/image/news-image/') }}"/>--}}
{{--                    </div>--}}

                    @if($errors->has('image'))
                        <span class="form-text">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                    @endif

                    <br>
                    {!! Form::label('joining_date', 'Joining Date <span class="text-danger">*</span>', [ "class" => "control-label {{ $errors->has('joining_date') ? 'has-error' : '' }} mt-3" ], false) !!}
                    {!! Form::date('joining_date', \Carbon\Carbon::now()->format('Y-m-d'), [ "class" => "form-control", 'required', 'autofocus' ]) !!}

                    @if($errors->has('joining_date'))
                        <span class="form-text">
                            <strong>{{ $erors->first('joining_date') }}</strong>
                        </span>
                    @endif

                    {!! Form::label('religion', 'Religion <span class="text-danger">*</span>', [ "class" => "control-label {{ $errors->has('religion') ? 'has-error' : '' }} mt-2" ],false) !!}
                    {!! Form::select('religion',[ 'Islam' => 'Islam', 'Hindu' => 'Hindu', 'Buddha', 'Christian' => 'Christian', 'Other' => 'Other' ], null, [ "class" => "form-control", 'required', 'autofocus' ]) !!}

                    @if($errors->has('religion'))
                        <span class="form-text">
                            <strong>{{ $errors->first('religion') }}</strong>
                        </span>
                    @endif

                    {!! Form::label('gender', 'Gender <span class="text-danger">*</span>', [ "class" => "control-label {{ $errors->has('gender') ? 'hae-error' : '' }} mt-2" ], false ) !!}
                    <br>
                    {!! Form::radio('gender', 'Male', true) !!}
                    Male
                    {!! Form::radio('gender', 'Female', false) !!}
                    Female

                    @if($errors->has('gender'))
                        <span class="form-text">
                            <strong>{{ $errors->first('gender') }}</strong>
                        </span>
                    @endif

                    <br>
                    {!! Form::label('marital_status', 'Marital Status <span class="text-danger">*</span>', [ "class" => "control-label {{ $errors->has('marital_status') ? 'has-error' : '' }} mt-3" ], false) !!}
                    <br>
                    {!! Form::radio('marital_status', 'Married', true) !!}
                    Married
                    {!! Form::radio('marital_status', 'Single', false) !!}
                    Single

                    @if($errors->has('marital_status'))
                        <span class="form-text">
                            <strong>{{ $errors->first('marital_status') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group col-md-12">
                    {!! Form::label('description', 'Description', [ "class" => "control-label {{ $errors->has('description') ? 'has-error' : '' }}" ]) !!}
                    {!! Form::textarea('description', null, [ "class" => "form-control", 'id' => 'description', 'autofocus' ]) !!}

                    @if($errors->has('description'))
                        <span class="form-text">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="form-group col-md-9">
            <label>Text</label>
            <input class="form-control" type="text" placeholder="Johnny Brown">
            <label class="mt-2">Email</label>
            <input class="form-control" value="bootstrap@example.com" type="email">
            <label class="mt-2">Telephone</label>
            <input class="form-control" value="1-(111)-111-1111" type="tel">
        </div>
        <div class="form-group col-md-3 text-right" width="100">
            <label class="text-left">Custom file input</label>
            <div class="custom-file text-center" width="100">
{{--                <div style="margin-top: 20px;">--}}
{{--                    <img id="output"   src="{{ asset('/public/uploads/image/news-image/') }}" />--}}
{{--                </div>--}}
                <input type="file" name="image" class="custom-file-input" id="image" onchange="loadFile(event)" >
                <label class="custom-file-label mt-lg-10 text-left">Choose file</label>
            </div>
        </div>
        <div class="form-group">
            <label>Email</label>
            <input class="form-control" value="bootstrap@example.com" type="email">
        </div>
        <div class="form-group">
            <label>URL</label>
            <input class="form-control" value="https://getbootstrap.com" type="url">
        </div>
        <div class="form-group">
            <label>Telephone</label>
            <input class="form-control" value="1-(111)-111-1111" type="tel">
        </div>
        <div class="form-group">
            <label>Password</label>
            <input class="form-control" value="password" type="password">
        </div>
        <div class="form-group">
            <label>Readonly input</label>
            <input class="form-control" type="text" placeholder="Readonly input here…" readonly>
        </div>
        <div class="form-group">
            <label>Disabled input</label>
            <input type="text" class="form-control" placeholder="Disabled input" disabled="">
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <label class="weight-600">Custom Checkbox</label>
                    <div class="custom-control custom-checkbox mb-5">
                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                        <label class="custom-control-label" for="customCheck1">Check this custom checkbox</label>
                    </div>
                    <div class="custom-control custom-checkbox mb-5">
                        <input type="checkbox" class="custom-control-input" id="customCheck2">
                        <label class="custom-control-label" for="customCheck2">Check this custom checkbox</label>
                    </div>
                    <div class="custom-control custom-checkbox mb-5">
                        <input type="checkbox" class="custom-control-input" id="customCheck3">
                        <label class="custom-control-label" for="customCheck3">Check this custom checkbox</label>
                    </div>
                    <div class="custom-control custom-checkbox mb-5">
                        <input type="checkbox" class="custom-control-input" id="customCheck4">
                        <label class="custom-control-label" for="customCheck4">Check this custom checkbox</label>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <label class="weight-600">Custom Radio</label>
                    <div class="custom-control custom-radio mb-5">
                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                        <label class="custom-control-label" for="customRadio1">Toggle this custom radio</label>
                    </div>
                    <div class="custom-control custom-radio mb-5">
                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                        <label class="custom-control-label" for="customRadio2">Or toggle this other custom radio</label>
                    </div>
                    <div class="custom-control custom-radio mb-5">
                        <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
                        <label class="custom-control-label" for="customRadio3">Or toggle this other custom radio</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Disabled select menu</label>
            <select class="form-control" disabled="">
                <option>Disabled select</option>
            </select>
        </div>
        <div class="form-group">
            <label>input plaintext</label>
            <input type="text" readonly class="form-control-plaintext" value="email@example.com">
        </div>
        <div class="form-group">
            <label>Textarea</label>
            <textarea class="form-control"></textarea>
        </div>
        <div class="form-group">
            <label>Help text</label>
            <input type="text" class="form-control">
            <small class="form-text text-muted">
                Your password must be 8-20 characters long, contain letters and numbers, and must not contain spaces, special characters, or emoji.
            </small>
        </div>
        <div class="form-group">
            <label>Example file input</label>
            <input type="file" class="form-control-file form-control height-auto">
        </div>
{{--        <div class="form-group">--}}
{{--            <label>Custom file input</label>--}}
{{--            <div class="custom-file">--}}
{{--                <input type="file" class="custom-file-input">--}}
{{--                <label class="custom-file-label">Choose file</label>--}}
{{--            </div>--}}
{{--        </div>--}}

    </div>
</div>
{{--@push('css')--}}
{{--<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">--}}
{{--<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>--}}
{{--@endpush--}}
{{--@push('js')--}}
{{--    <script>--}}
{{--        var loadFile = function(event) {--}}
{{--            alert('hi');--}}
{{--            var output = document.getElementById('output');--}}
{{--            output.src = URL.createObjectURL(event.target.files[0]);--}}
{{--        };--}}
{{--    </script>--}}
@push('js')
{{--    <script src="//cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script>--}}
<script src="{{ asset('backend/js/ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace('description', {
            uiColor: '#f1f1f1',
            extraPlugins: 'imageuploader',
        });
    </script>
@endpush
{{--@endpush--}}
