@extends('admin.layouts.master')

@section('content')
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="title">
                        <h4>Teacher Form</h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Create Teacher</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        {!! Form::open(['url' => 'admin/teachers']) !!}
        @include('admin.teachers.form')
        {!! Form::close() !!}
    </div>
@endsection
