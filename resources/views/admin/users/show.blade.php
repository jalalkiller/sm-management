@extends('admin.layouts.master')


@section('content')
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="title">
                        <h4>User Form</h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Show User</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <!-- Default Basic Forms Start -->
        <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
            <div class="clearfix">
                <div class="pull-left">
                    <h4 class="text-blue">Show user</h4>
                    <p class="mb-30 font-14">Admin pnale for sm</p>
                </div>
                <div class="pull-right">
                    <a href="{{ url('admin/users') }}" class="btn btn-primary btn-sm"  role="button"><i class="fa fa-arrow-left"></i> Back</a>
                </div>
            </div>

            <div class="form-group row">
                {!! Form::label('name','Name',["class" => "col-sm-12 col-md-2 col-form-label font-weight-bold"]) !!}
                <div class="col-sm-12 col-md-10">
                    {{ $user->name }}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('email','Email',["class" => "col-sm-12 col-md-2 col-form-label font-weight-bold"]) !!}
                <div class="col-sm-12 col-md-10">
                    {{ $user->email }}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('role','Role',["class" => "col-sm-12 col-md-2 col-form-label font-weight-bold"]) !!}
                <div class="col-sm-12 col-md-10">
                    @if(!empty($user->getRoleNames()))
                        @foreach($user->getRoleNames() as $v)
                            <label class="badge badge-success">{{ $v }}</label>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
