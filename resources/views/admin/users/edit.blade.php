@extends('admin.layouts.master')


@section('content')
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="title">
                        <h4>User Form</h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Edit User</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <!-- Default Basic Forms Start -->
        <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
            <div class="clearfix">
                <div class="pull-left">
                    <h4 class="text-blue">Edit user</h4>
                    <p class="mb-30 font-14">Admin pnale for sm</p>
                </div>
                <div class="pull-right">
                    <a href="{{ url('admin/users') }}" class="btn btn-primary btn-sm"  role="button"><i class="fa fa-arrow-left"></i> Back</a>
                </div>
            </div>
            {!! Form::model($user,['url' => ['admin/users',$user->id], 'method' => 'PATCH']) !!}
            <div class="form-group row">
                {!! Form::label('name','Name',["class" => "col-sm-12 col-md-2 col-form-label"]) !!}
                <div class="col-sm-12 col-md-10">
                    {!! Form::text('name',null,["class" => "form-control",'required','autofocus',"placeholder" => "Name"]) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('email','Email',["class" => "col-sm-12 col-md-2 col-form-label"]) !!}
                <div class="col-sm-12 col-md-10">
                    {!! Form::text('email',null,["class" => "form-control",'required','autofocus',"placeholder" => "Email"]) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('password','Password',["class" => "col-sm-12 col-md-2 col-form-label"]) !!}
                <div class="col-sm-12 col-md-10">
                    {!! Form::password('password',["class" => "form-control",'required','autofocus',"placeholder" => "Password"]) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('confirm_password','Confirm Password',["class" => "col-sm-12 col-md-2 col-form-label"]) !!}
                <div class="col-sm-12 col-md-10">
                    {!! Form::password('confirm-password',["class" => "form-control",'required','autofocus',"placeholder" => "Confirm Password"]) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('role','Role',["class" => "col-sm-12 col-md-2 col-form-label"]) !!}
                <div class="col-sm-12 col-md-10">
                    {!! Form::select('roles[]',$roles,$userRole,["class" => "form-control",'multiple']) !!}
                </div>
            </div>
            <div class="form-group row pull-right">
                <div class="col-sm-12 col-md-10">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!-- Default Basic Forms End -->


@endsection
