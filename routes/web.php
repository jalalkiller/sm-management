<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::resource('/teacherRegisters','Auth\TeacherRegisterController');
//Route::get('/dashboardTeachers','Auth\TeacherRegisterController@index');

Auth::routes();
Route::resource('/teacherRegisters','Auth\TeacherRegisterController');
Route::get('/dashboardTeachers','Auth\TeacherRegisterController@index');

Route::middleware('auth')->group(function (){
    Route::get('/', 'DashboardController@index');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('/admin/roles','RoleController');
    Route::resource('/admin/users','UserController');
    Route::resource('/admin/products','ProductController');
    Route::resource('/admin/teachers','TeacherController');
    Route::resource('/admin/subjects','SubjectController');
});
