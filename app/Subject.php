<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = ['subject_name','user_id'];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
}
