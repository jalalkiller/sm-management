<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->date('dob');
            $table->integer('mobile');
            $table->string('address');
            $table->string('subject');
            $table->string('image')->nullable();
            $table->text('description')->nullable();
            $table->date('joining_date');
            $table->enum('religion',['Islam','Hindu','Buddha','Christian','Other'])->default('Islam');
            $table->enum('marital_status',['Married','Single'])->default('Single');
            $table->enum('status',['Active','Inactive'])->default('Active');
            $table->boolean('active')->default(1)->change();
            $table->unsignedBigInteger('user_id');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
